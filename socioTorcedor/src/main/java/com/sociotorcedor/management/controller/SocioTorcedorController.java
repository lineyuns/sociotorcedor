package com.sociotorcedor.management.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sociotorcedor.management.domain.Campaign;
import com.sociotorcedor.management.exception.SocioTorcedorException;
import com.sociotorcedor.management.resource.SocioTorcedorJson;
import com.sociotorcedor.management.service.SaveUser;
import com.sociotorcedor.management.validator.SocioTorcedorHelper;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/api/v1/socioTorcedor")
@AllArgsConstructor
public class SocioTorcedorController {

	private SaveUser saveUser;
	private SocioTorcedorHelper socioTorcedorHelper;

	@ResponseStatus(HttpStatus.OK)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Campaign>> createUser(@RequestBody final SocioTorcedorJson socioTorcedorJson) {

		if (!socioTorcedorHelper.isUserAlreadyRegistered(socioTorcedorJson.toDomain().getEmail())) {
			log.info("saving new user");
			saveUser.execute(socioTorcedorJson.toDomain());

			log.info("associating new user");
			socioTorcedorHelper.associateSocioTorcedorCampaign(socioTorcedorJson.toDomain());

			return new ResponseEntity<>(HttpStatus.CREATED);
		}

		log.info("user already exists");

		if (!socioTorcedorHelper.isUserCampaignAssociationEmpty(socioTorcedorJson.getEmail())) {
			log.info("user already exists and has campaigns");
			throw new SocioTorcedorException("Usuario já possui cadastro e campanhas associadas");

		}

		log.info("show new campaign to user");
		return socioTorcedorHelper.showCampaignByTeamId(socioTorcedorJson.toDomain().getHeartTeam());

	}

}
