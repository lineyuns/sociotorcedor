package com.sociotorcedor.management.exception;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class SocioTorcedorException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;
	private Object[] msgParams;

    public SocioTorcedorException(String message) {
        super(message);
    }

    public SocioTorcedorException(String message, Throwable cause) {
        super(message, cause);
    }

    public SocioTorcedorException(String message, Object... msgParams) {
        super(message);
        this.msgParams = msgParams;
    }

    public SocioTorcedorException(String message, Throwable cause, Object[] msgParams) {
        super(message, cause);
        this.msgParams = msgParams;
    }

}
