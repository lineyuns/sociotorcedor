package com.sociotorcedor.management.service;

import org.springframework.stereotype.Service;

import com.sociotorcedor.management.domain.SocioTorcedor;
import com.sociotorcedor.management.mongo.repository.SocioTorcedorRepository;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@AllArgsConstructor
public class SaveUser {
	private SocioTorcedorRepository stRepository;

    public void execute(final SocioTorcedor socioTorcedor) {
        log.info("saving user, email: {}", socioTorcedor.getEmail());
        stRepository.save(socioTorcedor);
     }
}
