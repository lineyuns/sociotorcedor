package com.sociotorcedor.management.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@EnableMongoRepositories(basePackages = {"com.sociotorcedor.management.mongo.repository"})
@EnableMongoAuditing
public class MongoDBConfiguration {

}
