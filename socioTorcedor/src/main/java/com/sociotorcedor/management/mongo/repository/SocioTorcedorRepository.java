package com.sociotorcedor.management.mongo.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.sociotorcedor.management.domain.SocioTorcedor;

@Repository
public interface SocioTorcedorRepository extends MongoRepository<SocioTorcedor, String> {

}
