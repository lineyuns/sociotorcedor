package com.sociotorcedor.management.validator;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.sociotorcedor.management.domain.Campaign;
import com.sociotorcedor.management.domain.SocioTorcedor;
import com.sociotorcedor.management.mongo.repository.SocioTorcedorRepository;

import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class SocioTorcedorHelper {

	private SocioTorcedorRepository socioTorcedorRepository;

	public boolean isUserAlreadyRegistered(String email) {

		return socioTorcedorRepository.findById(email).isPresent();
	}

	public boolean isUserCampaignAssociationEmpty(String email) {

		List<Campaign> associationById = showAssociationByUserId(email);

		return associationById.isEmpty();
	}

/*	public boolean isCampaignAssociationUpdated(SocioTorcedor st) {
		return showNewCampaignByUser(st).isEmpty();
	}*/

	public ResponseEntity<List<Campaign>> showCampaignByTeamId(String teamId) {
		String url = "http://localhost:8090/api/v1/campaign/team/" + teamId;

		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<Campaign>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Campaign>>() {
				});

		return response;

	}

	public List<Campaign> showAssociationByUserId(String userEmail) {
		String url = "http://localhost:8090/api/v1/campaign/association/user/" + userEmail;
		RestTemplate restTemplate = new RestTemplate();

		ResponseEntity<List<Campaign>> response = restTemplate.exchange(url, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<Campaign>>() {
				});
		
		List<Campaign> stCampaign = response.getBody();

		return stCampaign;
	}

	public void associateSocioTorcedorCampaign(SocioTorcedor st) {
		String userId = st.getEmail();
		String teamId = st.getHeartTeam();
		String url = "http://localhost:8090/api/v1/campaign/association/" + userId + "/" + teamId;
		RestTemplate restTemplate = new RestTemplate();

		restTemplate.postForObject(url, null, SocioTorcedor.class);
	}

	/*public List<Campaign> showNewCampaignByUser(SocioTorcedor st) {
		List<SocioTorcedorCampaign> currentCpgAssociations = showAssociationByUserId(st.getEmail());
		ResponseEntity<List<Campaign>> currentCpgByTeamId = showCampaignByTeamId(st.getHeartTeam());
		return newCampaign;
	}*/
	
}
