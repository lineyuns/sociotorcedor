package com.sociotorcedor.management.resource;

import com.sociotorcedor.management.exception.SocioTorcedorException;

public class SocioTorcedorValidationException extends SocioTorcedorException{

	private static final long serialVersionUID = 1L;


	public SocioTorcedorValidationException(String message) {
        super(message);
    }

    public SocioTorcedorValidationException(String message, Throwable cause) {
        super(message, cause);
    }

    public SocioTorcedorValidationException(String message, Object... msgParams) {
        super(message, msgParams);
    }

    public SocioTorcedorValidationException(String message, Throwable cause, Object[] msgParams) {
        super(message, cause, msgParams);
    }
}
