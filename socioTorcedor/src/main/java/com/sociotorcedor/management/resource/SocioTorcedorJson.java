package com.sociotorcedor.management.resource;

import java.time.LocalDate;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.sociotorcedor.management.domain.SocioTorcedor;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SocioTorcedorJson {

	@NotEmpty(message = "Nome nao pode ser vazio.")
	private String name;
	@Email(message = "Informe um e-mail válido.")
	private String email;
	@NotNull(message = "A data de nascimento é obrigatória.")
	private LocalDate birthDate;
	@NotEmpty(message = "Time deve ser informado.")
	private String heartTeam;

	public SocioTorcedor toDomain() {
		SocioTorcedor socioTorcedor = new SocioTorcedor();
		socioTorcedor.setName(this.name);
		socioTorcedor.setEmail(this.email);
		socioTorcedor.setBirthDate(this.birthDate);
		socioTorcedor.setHeartTeam(this.heartTeam);

		return socioTorcedor;

	}
}
