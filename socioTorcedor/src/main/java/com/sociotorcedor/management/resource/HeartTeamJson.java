package com.sociotorcedor.management.resource;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
public class HeartTeamJson {

	@NonNull
	private String id;
	@NonNull
	private String name;

}
