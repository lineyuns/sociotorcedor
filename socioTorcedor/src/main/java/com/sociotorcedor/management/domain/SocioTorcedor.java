package com.sociotorcedor.management.domain;

import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "socioTorcedor")
@TypeAlias("SocioTorcedor")
public class SocioTorcedor {
	private String name;
	@Id
	@Indexed
	private String email;
	private LocalDate birthDate;
	private String heartTeam;
	

}
