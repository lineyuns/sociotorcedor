package com.sociotorcedor.management.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.TypeAlias;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "heartTeam")
@TypeAlias("HeartTeam")
public class HeartTeam {
	@Id
	@Indexed
	private String id;
	private String name;

	public HeartTeam toDomain() {
		HeartTeam heartTeam = new HeartTeam();
		heartTeam.setId(this.id);
		heartTeam.setName(this.name);

		return heartTeam;
	}
}
