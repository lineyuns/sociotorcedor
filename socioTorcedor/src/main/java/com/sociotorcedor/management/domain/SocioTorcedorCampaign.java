package com.sociotorcedor.management.domain;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SocioTorcedorCampaign {

	private String userEmail;

	private List<Campaign> campaignId;
}
