package com.sociotorcedor.management.domain;

import java.time.LocalDate;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;


@Data
@NoArgsConstructor
public class Campaign {

	@NonNull
	private String name;
	@NonNull
	private String teamId;
	@NonNull
	private LocalDate effectiveStartDate;
	@NonNull
	private LocalDate effectiveEndDate;

	public Campaign (Campaign campaign) {
		campaign.setName(this.name);
		campaign.setTeamId(this.teamId);
		campaign.setEffectiveStartDate(this.effectiveStartDate);
		campaign.setEffectiveEndDate(this.effectiveEndDate);

	}
}
