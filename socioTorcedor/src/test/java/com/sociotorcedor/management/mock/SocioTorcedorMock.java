package com.sociotorcedor.management.mock;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.sociotorcedor.management.domain.Campaign;
import com.sociotorcedor.management.domain.SocioTorcedor;
import com.sociotorcedor.management.domain.SocioTorcedorCampaign;

public class SocioTorcedorMock {

	public static SocioTorcedor createSocioTorcedor() {
		SocioTorcedor st = new SocioTorcedor();
		st.setName("Ana");
		st.setBirthDate(LocalDate.of(1993, 01, 25));
		st.setEmail("email@email.com");
		st.setHeartTeam("SPFC");
		return st;
	}

	public static Campaign createCampaign() {
		Campaign cpg = new Campaign();
		cpg.setName("Campanha1");
		cpg.setTeamId("SPFC");
		cpg.setEffectiveStartDate(LocalDate.of(2019, 01, 01));
		cpg.setEffectiveEndDate(LocalDate.of(2020, 01, 05));
		return cpg;
	}

	public static SocioTorcedorCampaign createAssociation() {
		SocioTorcedorCampaign stAssociation = new SocioTorcedorCampaign();
		stAssociation.setUserEmail("email@email.com");
		List<Campaign> campaignId = new ArrayList<Campaign>();
		Campaign cpg = createCampaign();
		campaignId.add(cpg);
		stAssociation.setCampaignId(campaignId);

		return stAssociation;

	}
}
