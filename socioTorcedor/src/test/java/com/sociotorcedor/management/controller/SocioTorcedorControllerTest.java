package com.sociotorcedor.management.controller;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.sociotorcedor.management.domain.SocioTorcedor;
import com.sociotorcedor.management.mock.SocioTorcedorMock;
import com.sociotorcedor.management.mongo.repository.SocioTorcedorRepository;
import com.sociotorcedor.management.service.SaveUser;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SocioTorcedorControllerTest {

	
	@Autowired
	private SocioTorcedorRepository socioTorcedorRepository;
	
	@MockBean
	private SaveUser saveUser;
	
	@MockBean
	SocioTorcedorMock stMock;

    @Test
	public void testFindSocioTorcedorByEmail() {
	
		SocioTorcedor st = SocioTorcedorMock.createSocioTorcedor();
		saveUser.execute(st); 
		String id = st.getEmail();

		Assert.assertTrue(socioTorcedorRepository.findById(id).isPresent());
		
	}

    
}
